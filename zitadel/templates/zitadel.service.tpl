[Unit]
Description=Zitadel
After=network.target network-online.target
Requires=network-online.target

[Service]
Type=simple
User={{zitadel_user}}
Group={{zitadel_group}}
Environment=ZITADEL_EXTERNALSECURE=false
ExecStart=/usr/local/bin/zitadel start-from-setup --masterkey "{{zitadel_masterkey}}" --tlsMode disabled --config /etc/zitadel/zitadel.conf

[Install]
WantedBy=multi-user.target