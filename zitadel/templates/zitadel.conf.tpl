ExternalDomain: {{zitadel_domain}}
ExternalSecure: {{zitadel_secure}}
ExternalPort: {{zitadel_port}}
Database:
  postgres:
    Host: {{zitadel_db_host}}
    Port: {{zitadel_db_port}}
    Database: {{zitadel_db_database}}
    MaxOpenConns: 25
    MaxConnLifetime: 1h
    MaxConnIdleTime: 5m
    Options:
    User:
      Username: {{zitadel_db_user}}
      Password: {{zitadel_db_password}}
      SSL:
        Mode: disable
        RootCert:
        Cert:
        Key:
#    Admin:
#      Username: zitadel
#      Password: zitadel
#      SSL:
#        Mode: disable
#        RootCert:
#        Cert:
#        Key: