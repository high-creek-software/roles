#! /bin/bash

cp /etc/letsencrypt/live/{{domain}}/fullchain.pem /var/lib/pgsql/data/server.crt
cp /etc/letsencrypt/live/{{domain}}/privkey.pem /var/lib/pgsql/data/server.key
chown postgres:postgres /var/lib/pgsql/data/server.crt /var/lib/pgsql/data/server.key
chmod 0644 /var/lib/pgsql/data/server.crt
chmod 0600 /var/lib/pgsql/data/server.key
systemctl restart postgresql