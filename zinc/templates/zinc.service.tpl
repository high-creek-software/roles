[Unit]
Description=Zinc server
After=network-online.target

[Service]
Restart=on-failure
User={{zinc_user}}
Group={{zinc_group}}
Environment=ZINC_FIRST_ADMIN_USER={{zinc_first_admin_name}}
Environment=ZINC_FIRST_ADMIN_PASSWORD={{zinc_first_admin_password}}
ExecStart=/usr/local/bin/zinc
Type=simple
WorkingDirectory=/var/run/zinc

[Install]
WantedBy=multi-user.target